/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <math.h>
#include <CImg.h>
#include <pthread.h>
#include <immintrin.h>
#define size 128

using namespace cimg_library;

// Data type for image components
// FIXME: Change this type according to your group assignment
typedef float data_t;

const char* SOURCE_IMG      = "bailarina.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";

//#define VECTOR_SIZE       18 // Array size. Note: It is not a multiple of 8
#define ITEMS_PER_PACKET size/(sizeof(data_t)*18)

//(sizeof(__m128)/sizeof(float))


int main() {
	// Open file and object initialization
	CImg<data_t> srcImage(SOURCE_IMG);
    
	data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
	data_t *pRdest, *pGdest, *pBdest;
	data_t *pDstImage; // Pointer to the new image pixels
	data_t Rmin, Rmax, Bmin, Bmax, Gmin, Gmax;
	uint width, height; // Width and height of the image
	uint nComp; // Number of image components
	struct timespec tStart, tEnd;
	double dElapsedTimes;
	float low = 0, high = 255;
	uint repeticiones=5;
   

	

	/***************************************************
	 * TODO: Variables initialization.
	 *   - Prepare variables for the algorithm
	 *   - This is not included in the benchmark time
	 */

	srcImage.display(); // Displays the source image
	width  = srcImage.width(); // Getting information from the source image
	height = srcImage.height();
	nComp  = srcImage.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)


	// Allocate memory space for destination image components
	pDstImage = (data_t *) malloc (width * height * nComp * sizeof(data_t));
	if (pDstImage == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}

	// Pointers to the componet arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;

	/***********************************************
	 * TODO: Algorithm start.
	 *   - Measure initial time
	 */
	if(clock_gettime(CLOCK_REALTIME, &tStart) == -1){
			perror("clock_gettime");
			exit(EXIT_FAILURE);
	}

	/************************************************
	 * FIXME: Algorithm.
	 * In this example, the algorithm is a components swap
	 *
	 * TO BE REPLACED BY YOUR ALGORITHM
	 */

	
	//Declaramos las variables min y max de cada componente
	Rmin = high;
	Rmax = low;
	Gmin = high;
	Gmax = low;
	Bmin = high;
	Bmax = low;
	//Calculamos el valor de las variables
	for (uint i = 0; i < width * height; i=i+ITEMS_PER_PACKET){
			if(*(pRsrc+i)>Rmax) Rmax=*(pRsrc+i);
			if(*(pRsrc+i)<Rmin) Rmin=*(pRsrc+i);
			if(*(pGsrc+i)>Gmax) Gmax=*(pGsrc+i);
			if(*(pGsrc+i)<Gmin) Gmin=*(pGsrc+i);
			if(*(pBsrc+i)>Bmax) Bmax=*(pBsrc+i);
			if(*(pBsrc+i)<Bmin) Bmin=*(pBsrc+i);
		}
	//Ejecutamos el algoritmo "repeticiones" veces.
	for(uint j=0;j<repeticiones;j++){
		
		for (uint i = 0; i < width * height; i=i+ITEMS_PER_PACKET){
			
			//Cargamos en memoria los punteros
            __m128 Rsrc= _mm_loadu_ps (pRsrc+i);
            __m128 Gsrc= _mm_loadu_ps (pGsrc + i);
            __m128 Bsrc= _mm_loadu_ps (pBsrc + i);
            __m128 Rdest= _mm_loadu_ps (pRdest + i);
            __m128 Gdest= _mm_loadu_ps (pGdest + i);
            __m128 Bdest= _mm_loadu_ps (pBdest + i);

			//Creamos las variables tipo __m128 con sus valores
            __m128 sRmax = _mm_set_ps1(Rmax);
            __m128 sRmin = _mm_set_ps1(Rmin);
            __m128 sGmax = _mm_set_ps1(Gmax);
            __m128 sGmin= _mm_set_ps1(Gmin);
            __m128 sBmax= _mm_set_ps1(Bmax);
            __m128 sBmin= _mm_set_ps1(Bmin);

			//Pasamos este algoritmo *(pRdest + i) = ((*(pRsrc + i)-Rmin)/(Rmax - Rmin))*Rmax+Rmin;
            Rdest=  _mm_mul_ps((_mm_div_ps(Rsrc-sRmin,sRmax - sRmin)),sRmax)+sRmin;
            Gdest=  _mm_mul_ps((_mm_div_ps(Gsrc-sGmin,sGmax - sGmin)),sGmax)+sGmin;
            Bdest=  _mm_mul_ps((_mm_div_ps(Bsrc-sBmin,sBmax - sBmin)),sBmax)+sBmin;
			//Guardamos el resultado obtenido de aplicar el algoritmo en el puntero dest
            _mm_storeu_ps(pRdest+i ,Rdest);
            _mm_storeu_ps(pGdest+i ,Gdest);
            _mm_storeu_ps(pBdest+i ,Bdest);

			//Ifs para controlar la saturacion (Rango 0 255)
			if(*(pRdest + i)>high){*(pRdest + i)=high;}
			if(*(pRdest + i)<low){*(pRdest + i)=low;}
			if(*(pGdest + i)>high){*(pGdest + i)=high;}
			if(*(pGdest + i)<low){*(pGdest + i)=low;}
			if(*(pBdest + i)>high){*(pBdest + i)=high;}
			if(*(pBdest + i)<low){*(pBdest + i)=low;}
		 }
	}
	
	/***********************************************
	 * TODO: End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 */
	if(clock_gettime(CLOCK_REALTIME, &tEnd) == -1){
			perror("clock_gettime");
			exit(EXIT_FAILURE);
	}

	// Show the elapsed time
	dElapsedTimes = (tEnd.tv_sec - tStart.tv_sec);
	dElapsedTimes += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
	printf("Elapsed time	: %f s.\n", dElapsedTimes);

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);

	// Store destination image in disk
	dstImage.save(DESTINATION_IMG); 

	// Display destination image
	dstImage.display();
	
	// Free memory
	free(pDstImage);

	return 0;
}
